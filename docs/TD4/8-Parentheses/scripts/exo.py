# --------- PYODIDE:code --------- #

ouvrant = "([{<"
fermant = ")]}>"
ouverture = {  # dictionnaire qui donne l'ouvrant en fonction du fermant
    ')': '(',
    ']': '[',
    '}': '{',
    '>': '<',
}

def est_bien_parenthesee(expression):
    ...

# --------- PYODIDE:corr --------- #

ouvrant = "([{<"
fermant = ")]}>"
ouverture = {}  # dictionnaire vide
for i in range(len(fermant)):
    ouverture[fermant[i]] = ouvrant[i]


def est_bien_parenthesee(expression):
    pile = []
    for c in expression:
        if c in ouvrant:
            pile.append(c)
        elif c in fermant:
            if pile == [] or pile.pop() != ouverture[c]:
                return False
    return pile == []

# --------- PYODIDE:tests --------- #

assert est_bien_parenthesee("(2 + 4)*7") == True
assert est_bien_parenthesee("tableau[f(i) - g(i)]") == True
assert est_bien_parenthesee(
    "int main(){int liste[2] = {4, 2}; return (10*liste[0] + liste[1]);}"
) == True

assert est_bien_parenthesee("(une parenthèse laissée ouverte") == False
assert est_bien_parenthesee("{<(}>)") == False
assert est_bien_parenthesee("c'est trop tard ;-)") == False

# --------- PYODIDE:secrets --------- #

assert est_bien_parenthesee("a(b)c") == True
assert est_bien_parenthesee("a[]b") == True
assert est_bien_parenthesee("{ }") == True
assert est_bien_parenthesee("()[]{}<>") == True
assert est_bien_parenthesee("([{<>}])") == True
assert est_bien_parenthesee("<{[([{<>}])]}>") == True
assert est_bien_parenthesee(
      "("*100 + "["*100 + "{"*100 + "<"*100
    + ">"*100 + "}"*100 + "]"*100 + ")"*100
) == True

assert est_bien_parenthesee("a(bc") == False
assert est_bien_parenthesee("a]b") == False
assert est_bien_parenthesee("} {") == False
assert est_bien_parenthesee("([)]{<}>") == False
assert est_bien_parenthesee("([{<}>)]") == False
assert est_bien_parenthesee("{<([[<{>}])]}>") == False
assert est_bien_parenthesee(
      "("*100 + "["*100 + "{"*100 + "<"*100
    + "<(>)"
    + ">"*100 + "}"*100 + "]"*100 + ")"*100
) == False
