!!!info "Autre correction possible"

    ```python
    def compte_caracteres_solution2(texte):
        """
        Paramètre : texte - Une chaîne de caractère ne contenant que des minuscules sans accent et des espaces.
        Sortie : resultat - Un dictionnaire dont les clefs sont les 27 caractères possibles et les valeurs le nombre de fois où ils apparaissent dans "texte".
        """
        # Le résultat est un dictionnaire, où les clefs sont tous les caractères
        # possibles, et les valeurs sont le nombre de fois où il apparaît dans texte.
        resultat = {}
        # La liste des 27 caractères à dénombrer.
        caracteres = 'abcdefghijklmnopqrstuvwxyz '
        
        for c in caracteres:
            # On parcourt les 27 caractères. Pour chacun, on initialise un compteur à 0. C'est la phase d'initialisation.
            resultat[c] = 0
            
        for lettre in texte:
            # On parcourt ensuite chaque lettre du texte et on incrémente la valeur de la clef correspondant à la lettre courante.
            resultat[lettre] += 1
            
        return resultat
    ```