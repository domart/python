# --------- PYODIDE:code --------- #

def occurrence_caracteres(phrase):
    ...

# --------- PYODIDE:corr --------- #

def occurrence_caracteres(phrase):
    # On créé un dictionnaire vide.
    occurrences = dict()
    for caractere in phrase:
        # On parcourt chaque caractère de la phrase.
        # Il faut distinguer deux cas :
        if caractere in occurrences:
            # 1. Le caractère courant est déjà dans le dictionnaire (donc on la déjà rencontré précédemment dans la phrase).
            # Dans ce cas, on augmente le compteur correspondant de 1.
            occurrences[caractere] += 1
        else:
            # 2. Le caractère courant n'est pas dans le dictionnaire, c'est-à-dire qu'on ne l'a jamais rencontré précédemment.
            # Dans ce cas, on créé l'entrée correspondante dans le dictionnaire, avec la valeur 1
            # (c'est la 1ère fois qu'on le rencontre).
            occurrences[caractere] = 1
    return occurrences

# --------- PYODIDE:tests --------- #

assert occurrence_caracteres("Bonjour à tous !") == {
    'B': 1, 'o': 3, 'n': 1, 'j': 1, 'u': 2, 'r': 1,
    ' ': 3, 'à': 1, 't': 1, 's': 1, '!': 1
}

assert occurrence_caracteres("ababbab") == {"a": 3, "b": 4}

# --------- PYODIDE:secrets --------- #

assert occurrence_caracteres("") == dict()
assert occurrence_caracteres("d") == {'d': 1}
assert occurrence_caracteres("d" * 100) == {'d': 100}
assert occurrence_caracteres("d" * 100 + "r" * 50) == {'d': 100, 'r': 50}