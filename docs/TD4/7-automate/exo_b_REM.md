On étudie ici des *automates finis déterministes* particuliers. Pour pouvoir traiter tous les automates finis déterministes, il manque la possibilité d'avoir plusieurs états finaux.

On peut montrer que ces automates permettent de représenter l'ensemble des [expressions régulières](https://fr.wikipedia.org/wiki/Expression_r%C3%A9guli%C3%A8re) (et réciproquement).

Les expressions régulières sont des chaînes de caractères permettant de décrire et donc d'identifier d'autres chaînes de caractères. Par exemple, l'automate cité dans l'énoncé correspond à l'expression régulière `(ab*c)*aa|b`.
