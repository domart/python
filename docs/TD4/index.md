# 📘 Présentation du TD4

L'objectif de ce TD est de travailler sur les dictionnaires (`#!python dict`).

## Ce qu'il faut savoir sur les dictionnaires :

???+success "Les dictionnaires"

    Un dictionaire est une liste de couples (clef, valeur). On utilise des accolades `{}` pour le définir. Les couples sont séparés par des `;`, et il y a un `:` entre une clef et sa valeur associée.

    Un dictionnaire correspond à une fonction en mathématiques. Les clefs sont les antécédents, et les valeurs sont les images.
    **Une clef ne peut donc pas être présente plus d'une fois**, mais une valeur peut être la même pour plusieurs clefs différentes.

    - Les clefs peuvent être de n'importe quel type, tant qu'il n'est pas mutable. On utilisera principalement des `#!python str` et des `#!python int`. Une `#!python list` ne peut pas être utilsée comme clef.
    - Les valeurs peuvent être de n'importe quel type.

    ```python title="Définir un dictionnaire"
    mondico={'blue':'bleu','green':'vert','red':'rouge','rot':'rouge', 'rojo':'rouge', 1:'un', 2:'deux'}
    print(f'mondico contient {mondico}')
    print(f'mondico contient {len(mondico)} couples')
    print(f'mondico est du type {type(mondico)}')
    ```

    On peut savoir si une clef est présente ou non dans un dictionnaire :

    ```python title="Savoir si une clef est présente"
    print('red' in mondico) # True  : clé "rouge" présente
    print('rouge' in mondico)   # False : cette clé n'existe pas
    ```

    On accède à une "valeur" du dictionnaire en utilisant la "clé"

    ```python title="Accéder à la valeur correspondant à une clef"
    print(mondico['blue'])
    print(mondico['purple']) # retourne une erreur
    ```
    
    Avant d'accéder à une valeur, on teste donc toujours si la clef est présente dans le dictionnaire :

    ```python title="Accéder à une valeur de manière sécurisée"
    if 'blue' in mondico:
        print(f"blue est une clef valide et la valeur associée est {mondico['blue']}")
    else:
        print("blue n'est pas une clef valide")

    if 'purple' in mondico:
        print(f"purple est une clef valide et la valeur associée est {mondico['purple']}")
    else:
        print("purple n'est pas une clef valide")
    ```

    On peut ajouter ou modifier un couple dans un dictionnaire. :warning: Attention, on ne peut pas modifier une clef !

    ```python title="Ajouter un couple ou modifier une valeur"
    mondico[3] = 'three'         # On ajoute un couple
    print(f'mondico contient maintenant {mondico}')
    mondico['purple'] = 'orange' # On ajoute un couple
    print(f'mondico contient maintenant {mondico}')
    mondico['purple'] = 'violet' # On modifie la valeur d'une clef existante
    print(f'mondico contient maintenant {mondico}')
    ```
    
    On peut supprimer un couple d'un dictionnaire

    ```python title="Supprimer un couple"
    valeur = mondico.pop('rojo') # supprime un élément à partir de sa clé
    print(f'mondico contient maintenant {mondico}')
    print(f'La valeur supprimée était {valeur}') # permet aussi de récupérer la valeur de cet élément
    ```
    
    Enfin, un `#!python dict` est un objet itérable. On peut donc le parcourir avec une boucle `#!python for`, soit pour accéder aux valeurs, soit pour les modifier.

    ```python title="Parcourir un dictionnaire"
    # Itérer sur les clés
    for key in mondico:
        print(f'La clef courante est {key}')
        print(f'La valeur correspondant à la clef {key} est {mondico[key]}') # permet de récupérer toutes les valeurs

    # Modification d'une valeur
    for key in mondico:
        mondico[key] = mondico[key] + " (en français)"
    
    print(f'mondico contient maintenant {mondico}')
    ```