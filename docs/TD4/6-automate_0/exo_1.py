

# --------- PYODIDE:env --------- #

def correspond(mot, automate, debut, fin):
    etat = debut
    for lettre in mot:
        if (etat, lettre) in automate:
            etat = automate[(etat, lettre)]
        else:
            return False
    return etat == fin

# --------- PYODIDE:code --------- #

# Cet automate reconnaît "tada" et "tadaaa" mais pas "tad" ni "taada"
automate_1 = ...
debut_1 = ...
fin_1 = ...

# --------- PYODIDE:corr --------- #

# Cet automate reconnaît "tada" et "tadaaa" mais pas "tad" ni "taada"
automate_1 = {(0, "t"): 1, (1, "a"): 2, (2, "d"): 3, (3, "a"): 4, (4, "a"): 4}
debut_1 = 0
fin_1 = 4

# --------- PYODIDE:tests --------- #

assert correspond("tada", automate_1, debut_1, fin_1) is True
assert correspond("tadaaa", automate_1, debut_1, fin_1) is True
assert correspond("tad", automate_1, debut_1, fin_1) is False
assert correspond("taada", automate_1, debut_1, fin_1) is False

# --------- PYODIDE:secrets --------- #


# tests secrets
def correspond(mot, automate, debut, fin):
    etat = debut
    for lettre in mot:
        if (etat, lettre) in automate:
            etat = automate[(etat, lettre)]
        else:
            return False
    return etat == fin

automate_1_corr = {(0, "t"): 1, (1, "a"): 2, (2, "d"): 3, (3, "a"): 4, (4, "a"): 4}
debut_1_corr = 0
fin_1_corr = 4
assert automate_1 == automate_1_corr
assert debut_1 == debut_1_corr
assert fin_1 == fin_1_corr

assert correspond("tadaaaaaaaaaaaaaaaaaaaa", automate_1, debut_1, fin_1) is True
assert correspond("tadaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa", automate_1, debut_1, fin_1) is True
assert correspond("atada", automate_1, debut_1, fin_1) is False
assert correspond("bbbaaaaaaaaaaaaaa", automate_1, debut_1, fin_1) is False
assert correspond("taaaaaaaaaaaada", automate_1, debut_1, fin_1) is False