

# --------- PYODIDE:env --------- #


def correspond(mot, automate, debut, fin):
    etat = debut
    for lettre in mot:
        if (etat, lettre) in automate:
            etat = automate[(etat, lettre)]
        else:
            return False
    return etat == fin



# --------- PYODIDE:code --------- #

# Cet automate reconnaît "bato" et "baato" mais pas "bota" (exemple du sujet)
automate_0 = {
    (0, "b"): 1,
    (1, "a"): 2,
    (2, "a"): 2,
    (2, "t"): 3,
    (3, "o"): 4,
    (4, "o"): 4,
}
debut_0 = 0
fin_0 = 4

# --------- PYODIDE:tests --------- #

assert correspond("bato", automate_0, debut_0, fin_0) is True
assert correspond("baato", automate_0, debut_0, fin_0) is True
assert correspond("bota", automate_0, debut_0, fin_0) is False