!!!info "Autres solutions possibles"

    1. **2 solutions**

        1. On crée tout d'abord une chaîne vide destinée à contenir la chaîne `mot` renversée. Ensuite, on ajoute chaque caractère de `mot` **au début** de `tom`.
        2. On utilise le _slicing_ pour ne conserver qu'une lettre sur 2.

        ```python
        def renverser_1(mot):
            tom = ''
            for caractere in mot:
                tom = caractere + tom
            return tom
            
        def renverser_2(mot):
            return mot[::-1]
        ```

    2. **Variante avec les indices**

        On pouvait aussi parcourir les indices de la fin vers le début.

        ```python
        def renverser(mot):
            tom = ''
            for i in range(len(mot)-1, -1, -1):
                tom = tom + mot[i]
            return tom
        ```

    3. **Variante fonctionnelle**

        Avec une liste par compréhension et on utilise `join` (un peu plus compliqué quand même ...) :

        ```python
        def renverser(mot):
            return ''.join([mot[i] for i in range(len(mot)-1, -1, -1)])
        ```
