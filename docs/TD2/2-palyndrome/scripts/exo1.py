# --------- PYODIDE:code --------- #

def renverser(mot):
    ...

# --------- PYODIDE:corr --------- #

def renverser(mot):
    # On créé une nouvelle chaîne de caractères "tom", initialisée à vide.
    tom = ''
    for caractere in mot:
        # Chaque caractère esr rajouté au début de cette chaîne.
        # Ainsi, la chaîne est écrite à l'envers à la fin.
        tom = caractere + tom
    return tom


# --------- PYODIDE:tests --------- #

assert renverser('informatique') == 'euqitamrofni'
assert renverser('python') == 'nohtyp'
assert renverser('') == ''

# --------- PYODIDE:secrets --------- #

assert renverser('abcde') == 'edcba'
assert renverser('159753') == '357951'
assert renverser('a6b-[]') == '][-b6a'
assert renverser('abc'*20) == 'cba'*20
