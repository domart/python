!!!info "Autres solutions possibles"

    1. **Version itérative**

        ```python
        def supprimheu(mots):
            discours = ""
            for mot in mots:
                if mot != "heu":
                    if discours != "":
                        # on a déjà mis un mot
                        discours += " "
                    discours += mot
            return discours
        ```

        On ajoute une espace avant d'ajouter un mot au discours, sauf si c'est le premier ; chose que l'on détecte quand le discours est vide.

    2. **Version fonctionnelle**

        ???+warning "Attention"
            L'énoncé interdit d'utiliser `join`

        Voici ce que ça aurait pu donner : 1 ligne !

        ```python
        def supprimheu(mots):
            return " ".join(mot for mot in mots if mot != "heu")
        ```

    3. **Version avec filtre avant collage**

        Cette version n'est pas recommandée !

        ```python
        def supprimeuh(mots):
            liste_mots = []
            for mot in mots:
                if mot != "heu":
                    liste_mots.append(mot)
            if liste_mots == []:
                return ''
            chaine = liste_mots[0]
            for mot in liste_mots[1:]:
                chaine = chaine + ' ' + mot
            return chaine
        ```

    

    Dernière remarque : si on souhaite après coup enlever les espaces en trop à la fin, on peut enlever le dernier caractère (après avoir vérifié que la chaine n'est pas vide !) :

    ```python
    chaine = chaine[:-1]
    ```
