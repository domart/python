# --------- PYODIDE:code --------- #

VOYELLES = 'aeiouy'

def dentiste(texte):
    ...

# --------- PYODIDE:corr --------- #

VOYELLES = 'aeiouy'

def dentiste(texte):
    # On créé une chaîne de caractères "resultat" initialisée à vide.
    # C'est ce qu'on renverra.
    resultat = ''
    for lettre in texte:
        # On parcourt chaque lettre du texte en paramètre.
        if lettre in VOYELLES:
            # Si la lettre est une voyelle, alors on l'ajoute à la chaîne résultat.
            resultat = resultat + lettre
            # Sinon, on ne fait rien (c'est-à-dire qu'on ne l'ajoute pas à la chaîne résultat).
    return resultat

# --------- PYODIDE:tests --------- #

assert dentiste("j'ai mal") == 'aia'
assert dentiste("il fait chaud") == 'iaiau'
assert dentiste("") == ''

# --------- PYODIDE:secrets --------- #

assert dentiste("a"*20 + "b"*10 + "e") == 'a'*20 + 'e'
assert dentiste("b"*10 + "e" + "a"*20) == 'e' + 'a'*20 
assert dentiste("ab"*10) == 'a'*10
assert dentiste("aeiouy"*10) == 'aeiouy'*10
assert dentiste("z"*100 + 'y') == 'y'