# --------- PYODIDE:code --------- #

def bien_parenthesee(expression):
    ...

# --------- PYODIDE:corr --------- #

def bien_parenthesee(expression):
    cpt = 0
    for caractere in expression:
        if caractere == '(':
            cpt += 1
        if caractere == ')':
            cpt -= 1
        if cpt < 0:
            return False
    return cpt == 0

# --------- PYODIDE:tests --------- #

assert bien_parenthesee('abc')
assert bien_parenthesee('(abc)')
assert bien_parenthesee('ab(cd)ef')
assert bien_parenthesee('a(b)c(d)e')
assert bien_parenthesee('a((b)c)d')
assert bien_parenthesee('a(b(c()e)f)g')
assert not bien_parenthesee('(')
assert not bien_parenthesee(')')
assert not bien_parenthesee('abc)')
assert not bien_parenthesee('ab)c')
assert not bien_parenthesee('a(b(c)d')
assert not bien_parenthesee('a(b)c)d(e)f')

# --------- PYODIDE:secrets --------- #

assert bien_parenthesee('abc')