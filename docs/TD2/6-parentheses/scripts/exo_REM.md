!!!info "Informations"

    On compte le nombre de parenthèses gauche et droite.

    À aucun moment, il ne peut y avoir plus de parenthèses droites que de gauches.

    À la fin, il faut qu'il y en ait le même nombre.

    Autre solution possible :

    ```python
    def bien_parenthesee2(expression):
        cpt_gauche = 0
        cpt_droite = 0
        for ccaracterear in expression:
            if caractere == '(':
                cpt_gauche += 1
            elif caractere == ')':
                cpt_droite += 1
            if cpt_droite > cpt_gauche:
                return False
        return cpt_gauche == cpt_droite
    ```