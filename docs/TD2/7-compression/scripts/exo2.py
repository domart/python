# --------- PYODIDE:code --------- #

def compresse(chaine):
    ...

# --------- PYODIDE:corr --------- #

def compresse(chaine):
    result = ''
    prec = chaine[0]
    cpt = 0
    for car in chaine:
        # On regarde si le caractère courant est répété plusieurs fois.
        if car == prec:
            cpt += 1
        else:
            while cpt >= 10:
                # Il y a un problème si le nombre de répétitions est strictement plus grand que 9.
                # Dans ce cas, s'il y a par exemple 'aaaaaaaaaaaa' (12 'a'), il faut renvoyer
                # '9a3a'.
                result += str(9) + prec
                cpt -= 9
            result += str(cpt) + prec
            cpt = 1
        prec = car
    while cpt >= 10:
        result += str(9) + prec
        cpt -= 9
    result += str(cpt) + prec
    return result

# --------- PYODIDE:tests --------- #

assert compresse('aaacaa9999') == '3a1c2a49'
assert compresse('aaaaa') == '5a'
assert compresse('33322233') == '333223'

# --------- PYODIDE:secrets --------- #

assert compresse('aaaaaaaaaaaa') == '9a3a'