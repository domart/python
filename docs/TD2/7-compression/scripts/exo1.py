# --------- PYODIDE:code --------- #

def decompresse(chaine):
    ...

# --------- PYODIDE:corr --------- #

def decompresse(chaine):
    result = ''
    for i in range(0, len(chaine), 2):
        # On parcourt la chaîne de caractères en paramètres.
        # i est ici l'indice dans cette chaîne.
        # On regarde à chaque fois deux caractères en même temps, c'est pour ca que dans le range, on avance de 2 en 2.
        # - chaine[i] contient le nombre de fois qu'il faut répéter le caractère souhaité.
        # - chaine[i+1] contient le caractère qu'il faut répéter.
        #
        # Pour rappel,
        # 3 * 'a'
        # est une chaîne de caractère dont l'évaluation donne 'aaa'.
        result += int(chaine[i]) * chaine[i+1]
    return result

# --------- PYODIDE:tests --------- #

assert decompresse('3a0b1c2a49') == 'aaacaa9999'
assert decompresse('5a') == 'aaaaa'
assert decompresse('333223') == '33322233'

# --------- PYODIDE:secrets --------- #

assert decompresse('9a3a') == 'aaaaaaaaaaaa'