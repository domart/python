# --------- PYODIDE:code --------- #

def nb_max_consecutifs(motif, phrase):
    ...

# --------- PYODIDE:corr --------- #

def nb_max_consecutifs(motif, phrase):
    # Pour répondre à ce problème, il faut deux compteur :
    # 1. nb_max qui contiendra le plus grand nombre du motif dans la phrase.
    # 2. nb_courant qui contiendra le nombre de répétition lorsqu'on trouve le motif en parcourant la chaîne.
    nb_max = 0
    nb_courant = 0
    for caractere in phrase:
        # On parcourt chaque caractère de la chaîne.
        if caractere == motif:
            # Le caractère courant est le motif recherché.
            # On augmente donc de 1 le nombre de répétition de ce motif.
            nb_courant += 1
            if nb_courant > nb_max:
                # Si le nombre de répétitions du motif sur lequel on se trouve est plus grand
                # que le plus grand jusqu'alors, on met à jour ce "plus grand".
                nb_max = nb_courant
        else:
            # Le caractère courant n'est pas le motif recherché.
            # On remet à 0 le nombre de répétition du motif.
            nb_courant = 0
    return nb_max

# --------- PYODIDE:tests --------- #

phrase = "Dans une phrase !!! écrite !!! certains utilisateurs abusent des points d'exclamations !! Ce pour différentes raisons ! Bref."
assert nb_max_consecutifs("!", phrase) == 3

phrase = "Un mot    puis        un        autre avec espaces."
assert nb_max_consecutifs(" ", phrase) == 8

expression = "((2 * x + 3) / (x + 1))"
assert nb_max_consecutifs("(", expression) == 2
assert nb_max_consecutifs("-", expression) == 0

# --------- PYODIDE:secrets --------- #

assert nb_max_consecutifs("!", "!!!! !!! !! !") == 4
assert nb_max_consecutifs("!", "! !! !!! !!!! !!! !! !") == 4
assert nb_max_consecutifs("!", "! !! !!! !!!!") == 4
assert nb_max_consecutifs("-", "---") == 3
assert nb_max_consecutifs("-", " ---") == 3
assert nb_max_consecutifs("-", " --- ") == 3
assert nb_max_consecutifs("-", "--- ") == 3