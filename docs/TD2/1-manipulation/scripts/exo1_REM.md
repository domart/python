!!!info "Autre solution possible"

    Voici une autre solution possible :

    ```python
    def derniere_lettre2(mot):
        # On gère les cas particuliers : le mot ne contient
        # aucune lettre, ou qu'une seule.
        if len(mot) <= 1:
            return mot
        return mot[-1]
    ```