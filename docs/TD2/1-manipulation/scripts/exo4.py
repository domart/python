# --------- PYODIDE:code --------- #

def assemble(liste):
    ...


# --------- PYODIDE:corr --------- #

def assemble(liste):
    # On fait le contraire de précédemment.
    # On réunit les différents éléments de la liste, en mettant
    # une chaîne de caractère entre chaque.
    return ' '.join(liste)


# --------- PYODIDE:tests --------- #

assert assemble(['informatique']) == 'informatique'
assert assemble('') == ''
assert assemble(['Engage', 'le', 'jeu', 'que', 'je', 'le', 'gagne']) == 'Engage le jeu que je le gagne'

# --------- PYODIDE:secrets --------- #

assert assemble('') == ''
