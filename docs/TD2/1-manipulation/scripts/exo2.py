# --------- PYODIDE:code --------- #

def moitie(mot):
    ...

# --------- PYODIDE:corr --------- #

def moitie(mot):
    # On utilise ici la technique du slicing.
    return mot[::2]



# --------- PYODIDE:tests --------- #

assert moitie('informatique') == 'ifraiu'
assert moitie('kayak') == 'kyk'
assert moitie('') == ''
assert moitie('Engage le jeu que je le gagne') == 'Egg ejuqej egge'

# --------- PYODIDE:secrets --------- #
assert moitie('informatique') == 'ifraiu'
