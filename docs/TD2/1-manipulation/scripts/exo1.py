# --------- PYODIDE:code --------- #

def derniere_lettre(mot):
    ...

# --------- PYODIDE:corr --------- #

def derniere_lettre(mot):
    dernier = ''
    for caractere in mot:
        dernier = caractere
    return dernier


# --------- PYODIDE:tests --------- #

assert derniere_lettre('informatique') == 'e'
assert derniere_lettre('python') == 'n'
assert derniere_lettre('') == ''


# --------- PYODIDE:secrets --------- #

assert derniere_lettre('informatique') == 'e'
