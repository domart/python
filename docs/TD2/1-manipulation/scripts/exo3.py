# --------- PYODIDE:code --------- #

def separe(mot):
    ...


# --------- PYODIDE:corr --------- #

def separe(mot):
    # Il faut commencer par supprimer les espaces
    # qu'il peut y avoir en trop au début ou à la fin.
    mot = mot.strip()
    return mot.split(' ')


# --------- PYODIDE:tests --------- #


assert separe('informatique') == ['informatique']
assert separe('') == ['']
assert separe('Engage le jeu que je le gagne') == ['Engage', 'le', 'jeu', 'que', 'je', 'le', 'gagne']

# --------- PYODIDE:secrets --------- #

assert separe('') == ['']
