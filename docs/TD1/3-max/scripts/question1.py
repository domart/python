# --------- PYODIDE:code --------- #

def plus_grand_parmi_2():
    ...


# --------- PYODIDE:corr --------- #

def plus_grand_parmi_2(a, b):
    if a > b:
        return a
    else:
        return b


# --------- PYODIDE:tests --------- #

assert plus_grand_parmi_2(1, 2) == 2
assert plus_grand_parmi_2(-1, -2) == -1


# --------- PYODIDE:secrets --------- #

assert plus_grand_parmi_2(1, 2) == 2
assert plus_grand_parmi_2(-1, -2) == -1
