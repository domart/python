# --------- PYODIDE:code --------- #

def plus_grand_parmi_3():
    ...


# --------- PYODIDE:corr --------- #

def plus_grand_parmi_3(a, b, c):
    if a > b and a > c:
        return a
    elif b > c:
        return b
    else:
        return c


# --------- PYODIDE:tests --------- #

assert plus_grand_parmi_3(1, 2, 3) == 3
assert plus_grand_parmi_3(-1, 2, -3) == 2
assert plus_grand_parmi_3(-1, -2, -3) == -1


# --------- PYODIDE:secrets --------- #

assert plus_grand_parmi_3(1, 2, 3) == 3
