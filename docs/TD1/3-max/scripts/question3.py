# --------- PYODIDE:code --------- #

def plus_grand_parmi_n():
    ...


# --------- PYODIDE:corr --------- #

def plus_grand_parmi_n(liste):
    # Cas particulier : la liste est vide.
    if len(liste) == 0:
        return None
    
    # Dans tous les autres cas, on initialise la variable maximum
    # avec le premier élément de la liste.
    maximum = liste[0]
    for element in liste:
        if element > maximum:
            # Dès qu'on trouve un élément plus grand,
            # on met à jour la variable maximum.
            maximum = element
    return maximum


# --------- PYODIDE:tests --------- #

assert plus_grand_parmi_n([-5]) == -5
assert plus_grand_parmi_n([1, 2, 3, -5, 1]) == 3
assert plus_grand_parmi_n([]) == None


# --------- PYODIDE:secrets --------- #

assert plus_grand_parmi_n([5]) == 5
