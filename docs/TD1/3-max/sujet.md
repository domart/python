---
author: Benoît Domart
title: Exercice 3 - Maximum
tags:
  - 0-simple
  - 1-condition
  - 1-boucle
---
# Exercice 3 - Maximum

???+warning "Attention"
    Dans cet exercice, on interdit d'utiliser les fonctions `max`, `sort` et `sorted`.

## 1. Maximum de deux nombres

Compléter le script suivant pour que la fonction `plus_grand_parmi_2` prenne deux nombres en paramètres et renvoie le plus grand des deux :

{{ IDE('scripts/question1', SANS = "max, sorted, sort") }}

## 2. Maximum de trois nombres

Compléter le script suivant pour que la fonction `plus_grand_parmi_3` prenne trois nombres en paramètres et renvoie le plus grand :

{{ IDE('scripts/question2', SANS = "max, sorted, sort") }}

## 2. Maximum de n nombres

Compléter le script suivant pour que la fonction `plus_grand_parmi_n` prenne une liste de nombres (éventuellement vide) en paramètres et renvoie le plus grand :

???+note "Rappel"
    Pour rappel, en Python, `[]` est la liste vide.

{{ IDE('scripts/question3', SANS = "max, sorted, sort") }}