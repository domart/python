# --------- PYODIDE:code --------- #

def quadruple(a):
    return ... # compléter ici


# --------- PYODIDE:corr --------- #

def quadruple(a):
    return double(double(a))


# --------- PYODIDE:tests --------- #

assert quadruple(10) == 40
assert quadruple(7) == 28


# --------- PYODIDE:secrets --------- #

assert quadruple(-10) == -40
assert quadruple(1700) == 3400*2