# --------- PYODIDE:code --------- #

def double(a):
    return ... # compléter ici


# --------- PYODIDE:corr --------- #

def double(a):
    return somme(a, a)


# --------- PYODIDE:tests --------- #

assert double(10) == 20
assert double(7) == 14


# --------- PYODIDE:secrets --------- #

assert double(-10) == -20
assert double(1700) == 3400