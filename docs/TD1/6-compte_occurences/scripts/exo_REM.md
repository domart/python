!!!info "Autres solutions possibles"

    2. **Itération avec indice**

        Version possible ; peu d'intérêt

        ```python
        def compte_occurrences(cible, mot):
            nb_occurrences = 0
            for i in range(len(mot)):
                if mot[i] == cible:
                    nb_occurrences += 1
            return nb_occurrences
        ```

    3. **Version fonctionnelle**

        Pour les bons élèves.

        ```python
        def compte_occurrences(cible, mot):
            return sum(1 for lettre in mot if lettre == cible)
        ```

    4. **Version non autorisée**

        Avec la facilité du langage Python (`count`).

        ```python
        def compte_occurrences(cible, mot):
            return mot.count(cible)
        ```
