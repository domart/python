# --------- PYODIDE:code --------- #

# Exemple pour a contenant 10 et b contenant 3,
# mais le script doit fonctionner avec n'importe quelles valeurs.
a = 10
b = 3

...


# --------- PYODIDE:corr --------- #

a = 10
b = 3
c = a
a = b
b = c


# --------- PYODIDE:tests --------- #

assert a == 3
assert b == 10


# --------- PYODIDE:secrets --------- #

assert a == 3
assert b == 10