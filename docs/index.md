# 🏡 Accueil

**Exercices pour les TDs des 3A du DMS.**

!!!success "Fonctionnement du site"

    Ce site propose un certain nombre d'exercices de Python dont l'objectif est de maîtriser les bases de la programmation et les bases de ce langage. Des exercices nécessitant plus de réflexions sont signalés par une ou plusieurs ⭐.

    Il propose également une dernière section pour ceux qui maîtrisent déjà ces bases, pour leurs permettre d'aller plus loin.

    Chaque exercice vient avec un *valideur* automatique. En effet, à chaque exercice est associé un certain nombre des tests, qui sont effectués automatiquement (ces sont les `assert ...`).

    Sous l'éditeur de code, plusieurs boutons sont disponibles, notamment :

    - ![](assets/images/icons8-play-64.png){ .md-button .md-button--primary} **Exécution :** les tests visibles sont exécutés. Les éventuelles erreurs sont alors indiquées.
    - ![](assets/images/icons8-check-64.png){ .md-button .md-button--primary} **Validation :** les tests cachés sont exécutés. Les éventuelles erreurs sont alors indiquées.
        
        S'il n'y en a pas, la solution est affichée juste en dessous (il faut cliquer sur le titre pour ouvrir le bloc) de cette façon :

        ???tip "Solution"

            La solution est affichée ici.
        
        Des éventuelles explications supplémentaires peuvent être indiquées.
    
    ???+success "Réussir un exercice"

        Un exercice est considéré comme réussi lorsque le code a été **exécuté** puis **validé** (sans erreur), c'est-à-dire lorsque la correction a été affichée.

        Même lorsque tu as réussi l'exercice, il est fortement conseillé de regarder la correction et les éventuels commentaires. Ils peuvent permettre de mieux comprendre certains points.

    ???+success "Afficher la solution quand on ne trouve pas ..."

        Il est possible d'afficher la correction même si on n'a pas réussi l'exercice.

        Il y a en effet un compteur qui est décrémenté à chaque clic sur le bouton de validation. Lorsque ce compteur arrive à 0, la correction est affichée.

        Pour obtenir la correction, il suffit dont de cliquer sur valider autant de fois que nécessaire !
    
    ???+tip "Phase de réflexion"

        Pour les exercices complexes, il est fortement conseillé d'utiliser un *vrai* éditeur Python, comme Spyder que nous utiliserons en TP.

        Une fois que vous pensez avoir la solution, il suffit de la coller dans ce site pour vérifier que la validation fonctionne !



!!!info "Informations"

    Beaucoup d'emprunts ont été effectués à [ce regroupement d'exercices pour NSI](https://e-nsi.gitlab.io/pratique/){target=_blank}

    Ce site est sous licence <a
        href="http://creativecommons.org/licenses/by-nc-sa/4.0/?ref=chooser-v1"
        target="_blank" rel="license noopener noreferrer"
        style="display:inline-block;">CC BY-NC-SA 4.0<img
        style="height:22px!important;margin-left:3px;vertical-align:text-bottom;"
        src="https://mirrors.creativecommons.org/presskit/icons/cc.svg?ref=chooser-v1"><img
        style="height:22px!important;margin-left:3px;vertical-align:text-bottom;"
        src="https://mirrors.creativecommons.org/presskit/icons/by.svg?ref=chooser-v1"><img
        style="height:22px!important;margin-left:3px;vertical-align:text-bottom;"
        src="https://mirrors.creativecommons.org/presskit/icons/nc.svg?ref=chooser-v1"><img
        style="height:22px!important;margin-left:3px;vertical-align:text-bottom;"
        src="https://mirrors.creativecommons.org/presskit/icons/sa.svg?ref=chooser-v1"></a>