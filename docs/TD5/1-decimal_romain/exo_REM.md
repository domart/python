On procède de façon analogue au « Rendu de monnaie » : au lieu de rendre en priorité les « pièces » les plus grandes, ici, on écrit en priorité les « valeurs » les plus grandes.

```text title="📋 Explications"
Créer une chaîne de caractère vide et l'affecter à la variable résultat
Pour chaque nombre utilisable :
    Tant que la valeur à convertir est supérieure ou égale à ce nombre :
        Concaténer l'écriture romaine de ce nombre au résultat
        Soustraire ce nombre à la valeur à convertir
```

On peut aussi procéder en utilisant des divisions euclidiennes afin de ne pas écrire de boucle `#!py while`. On rappelle en effet que :

* `#!py 1748 // 1000` est évalué à `#!py 1`, c'est le quotient de la division euclidienne de $1\,748$ par $1\,000$ ;
* `#!py 1748 %  1000` est évalué à `#!py 748`, c'est le reste de cette même division.

```python
def romain(valeur):
    resultat = ""
    for entier, symbole in VALEURS:
        quotient = valeur // entier
        valeur = valeur % entier
        resultat += symbole * quotient  # concaténation de caractères
    return resultat
```

Il est aussi possible de travailler avec un dictionnaire `{valeur: symbole}`. Cette approche pose néanmoins la question de l'ordre du parcours d'un dictionnaire. Il est en effet indispensable de parcourir les couples `(valeur, symbole)` dans l'ordre décroissant des valeurs.

Depuis Python 3.7, les parcours des dictionnaires sont effectués dans l'ordre d'insertion des clés. On peut donc procéder ainsi :

```python
DICO_VALEURS = {
    1000: "M", 900: "CM", 500: "D", 400: "CD",
    100: "C", 90: "XC", 50: "L", 40: "XL",
    10: "X", 9: "IX", 5: "V", 4: "IV",
    1: "I"
}

def romain(valeur):
    resultat = ""
    for entier, symbole in DICO_VALEURS:
        while valeur >= entier:
            valeur -= entier
            resultat += DICO_VALEURS[entier]
    return resultat
```

Il faut toutefois garder à l'esprit que cette approche dépend de la version de Python utilisée : elle n'est pas généralisable ni à l'ensemble des versions de Python ni **à tous les langages de programmation**.

!!! note "Et après ?"

    Les entiers supérieurs ou égaux à $5\,000$ s'écrivent en utilisant une barre horizontale indiquant qu'il faut multiplier le facteur concerné par $1\,000$.

    Par exemple $\overline{\text{VI}}\text{CCCVIII}$ représente $6\,308$ car le $\overline{\text{VI}}$ est multiplié par $1\,000$.
