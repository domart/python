La correction propose une méthode qui calcule les poids du poids faible vers le poids fort.

Il est possible d'utiliser un algorithme glouton, qui calcule les différents poids du poids fort vers le poids faible.

```python
def en_shadok(nombre):
    VALEURS = ["GA", "BU", "ZO", "MEU"]
    if nombre == 0:
        return "GA"
    resultat = ""
    puissance = 4 ** 7
    while puissance > 0:
        chiffre = VALEURS[nombre // puissance]
        nombre = nombre % puissance
        if not (chiffre == "GA" and resultat == ""):
            resultat = resultat + chiffre
        puissance = puissance // 4
    return resultat
```

Ce problème s'apparente à un problème de rendu de pièces où le système de pièces serait $[4^7, 4^6, 4^5, 4^4, 4^3, 4^2, 4^1, 1]$.

Avec un tel système de *pièces* un algorithme glouton fournit toujours l'unique solution au problème.
