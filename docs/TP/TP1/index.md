# TP 1 - Manipulation de fichiers & de dictionnaires

## 1. Le sujet

<center>
    [Sujet en PDF](./fichiers/TP1.pdf){ .md-button download="TP1.pdf" }
</center>

## 2. Les fichiers de données à télécharger

<center>
    [fichiers.zip](./fichiers/fichiers.zip){ .md-button download="fichiers.zip" }
</center>