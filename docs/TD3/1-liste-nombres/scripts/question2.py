# --------- PYODIDE:code --------- #

def somme(liste):
    ...

# --------- PYODIDE:corr --------- #

def somme(liste):
    s = 0
    for element in liste:
        s += element
    return s

# --------- PYODIDE:tests --------- #

assert somme([]) == 0
assert somme([5]) == 5
assert somme([2, -4, -5, 2, 7]) == 2

# --------- PYODIDE:secrets --------- #

assert somme([]) == 0
