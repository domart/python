# --------- PYODIDE:code --------- #

def extremums(liste):
    ...

# --------- PYODIDE:corr --------- #

def extremums(liste):
    if len(liste) == 0:
        return []
    M = liste[0]   # Le maximum.
    m = liste[0]   # Le minimum.
    for element in liste:
        if element > M:
            M = element
        if element < m:
            m = element
    return [m, M]

# --------- PYODIDE:tests --------- #

assert extremums([]) == []
assert extremums([5]) == [5, 5]
assert extremums([2, -4.3, -5, 2, 7]) == [-5, 7]

# --------- PYODIDE:secrets --------- #

assert extremums([]) == []
