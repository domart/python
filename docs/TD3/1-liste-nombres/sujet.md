---
author: Benoît Domart
title: Exercice 1 - Liste de nombres
tags:
  - 1-boucle
  - 2-liste
---
# Exercice 1 - Liste de nombres

## 1. Extremums

Compléter la fonction `extremums` pour qu'elle renvoie une liste contenant, dans cet ordre, la plus petite et la plus grande valeur de la liste de nombres en paramètre.

Si la liste en paramètre est vide, la fonction doit alors renvoyer une liste vide.

???+warning "Attention"
    Il ne faut pas utiliser les fonctions `max` et `min`.

{{ IDE('scripts/question1', SANS='max, min') }}

## 2. Somme

Compléter la fonction `somme` pour qu'elle renvoie la somme des éléments de la liste de nombres en paramètre.

???+warning "Attention"
    Il ne faut pas utiliser la fonction `sum`.

{{ IDE('scripts/question2', SANS='sum') }}

## 3. Moyenne

Compléter la fonction `moyenne` pour qu'elle renvoie la moyenne des éléments de la liste de nombres en paramètre.

???+warning "Attention"
    Il ne faut pas utiliser la fonction `sum`.

{{ IDE('scripts/question3', SANS='sum') }}