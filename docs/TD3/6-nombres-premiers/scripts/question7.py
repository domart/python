# --------- PYODIDE:code --------- #

def prochain_premier(n):
    ...


# --------- PYODIDE:corr --------- #

def prochain_premier(n):
    # On cherche le premier entier premier en partant de n.
    while not est_premier(n):
        n += 1
    return n

# --------- PYODIDE:tests --------- #

assert prochain_premier(0) == 2
assert prochain_premier(1) == 2
assert prochain_premier(2) == 2
assert prochain_premier(3) == 3
assert prochain_premier(10) == 11
assert prochain_premier(24) == 29
assert prochain_premier(32) == 37

# --------- PYODIDE:secrets --------- #

assert prochain_premier(0) == 2
