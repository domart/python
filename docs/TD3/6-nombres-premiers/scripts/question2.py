# --------- PYODIDE:code --------- #

def est_premier(n):
    ...

# --------- PYODIDE:corr --------- #

def est_premier(n):
    # On gère les particuliers où n est strictement plus petit que 2.
    if n < 2:
        return False
    # Pour les autres cas, on regarde si n est divisible par un entier entre 2 et n-1.
    # Dès qu'on trouve un diviseur, c'est que n n'est pas premier.
    for i in range(2, n):
        if n%i == 0:
            return False
    # Si aucun entier ne divise n, c'est que n est premier.
    return True

# --------- PYODIDE:tests --------- #

assert est_premier(2) == True
assert est_premier(3) == True
assert est_premier(4) == False
assert est_premier(5) == True
assert est_premier(6) == False
assert est_premier(7) == True
assert est_premier(15) == False
assert est_premier(23) == True

# --------- PYODIDE:secrets --------- #

assert est_premier(0) == False
assert est_premier(1) == False