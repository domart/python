# --------- PYODIDE:code --------- #

def est_pair(n):
    ...

# --------- PYODIDE:corr --------- #

def est_pair(n):
    return n%2 == 0

# --------- PYODIDE:tests --------- #

assert est_pair(0)
assert est_pair(2)
assert est_pair(4)
assert est_pair(10)
assert not est_pair(1)
assert not est_pair(3)
assert not est_pair(5)
assert not est_pair(11)

# --------- PYODIDE:secrets --------- #

assert est_pair(0)
