# --------- PYODIDE:code --------- #

def inverse_persistance_multiplicative(n):
    ...

# --------- PYODIDE:corr --------- #

def inverse_persistance_multiplicative(n):
    # On cherche la plus petite valeur de l'entier result dont la persistance multiplicative est supérieure ou égale à n.
    # On commence à 0 ...
    result = 0
    while persistance_multiplicative(result) < n:
        # ... et on augemnte de 1 jusqu'à trouver.
        result += 1
    return result

# --------- PYODIDE:tests --------- #

assert inverse_persistance_multiplicative(0) == 0
assert inverse_persistance_multiplicative(1) == 10
assert inverse_persistance_multiplicative(2) == 25
assert inverse_persistance_multiplicative(3) == 39
assert inverse_persistance_multiplicative(7) == 68889

# --------- PYODIDE:secrets --------- #

assert inverse_persistance_multiplicative(0) == 0
