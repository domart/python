!!!info "Information"

    Ici, il est très pratique (mais pas obligatoire) de créer une fonction intermédiaire, `multiplication_chiffre`, qui renvoi le produit des chiffres d'un nombre donné.