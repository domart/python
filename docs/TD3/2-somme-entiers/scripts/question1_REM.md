!!!info "Autre solution possible"

    ```python
    def somme_de_1_a_n_bis(n):
        '''
        Une correction avec la fonction sum (interdite ici).
        '''
        return sum([i for i in range(n+1)])
    ```