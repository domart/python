Il serait aussi possible de se passer de l'indice `j` en parcourant directement les valeurs de chaque ligne :

```python
def ajoute_colonne(tab):
    hauteur = len(tab)
    for i in range(hauteur):
        somme = 0
        for x in tab[i]:
            somme += x
        tab[i].append(somme)
```

En poursuivant cette idée et en profitant du caractère mutable des listes **Python**, on peut aussi se passer complètement
d'indices :

```python
def ajoute_colonne(tab):
    for ligne in tab:
        somme = 0
        for x in ligne:
            somme += x
        ligne.append(somme)
```
