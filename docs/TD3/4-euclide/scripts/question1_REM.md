!!!info "Autre solution possible"

    En utilisant l'affectation multiple, la fonction est beaucoup plus simple !

    ```python
    def euclide(a, b):
        while b != 0:
            a, b = b, a%b
        return a
    ```