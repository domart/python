# --------- PYODIDE:code --------- #

def euclide(a, b):
    ...

# --------- PYODIDE:corr --------- #

def euclide(a, b):
    while b != 0:
        # On stocke la valeur de b dans une variable temporaire, pour pouvoir la réutiliser par la suite.
        temp = b
        # On calcule le reste de la division de a par b.
        # C'est la nouvelle valeur de b.
        b = a%b
        # La nouvelle valeur de a est l'ancienne valeur de b.
        a = temp
    return a

# --------- PYODIDE:tests --------- #

assert euclide(10, 1) == 1
assert euclide(10, 6) == 2
assert euclide(10, 5) == 5
assert euclide(10, 7) == 1
assert euclide(221, 782) == 17
assert euclide(223, 782) == 1

# --------- PYODIDE:secrets --------- #

assert euclide(10, 1) == 1
